# README #

Download from yande.re

Input: .txt file containing list of tags

ex. 

```
# comment
tag1
tag2
...
```

Output: downloaded files

# Requirements #

Python 3.7

# Usage #
```
python main.py "C:/path/to/list.txt" "A:/yande.re/"
```

It is ok to interrupt/terminate the process.  Just run it again.

Queries are auto-paginated.  Existing files are checked for md5.  Incorrectly downloaded files are re-downloaded on the fly.