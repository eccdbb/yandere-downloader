import xmltodict
import pprint
import requests
import urllib.parse
import os
import hashlib
import argparse
import logging
import time
import threading
import queue
import random

class Downloader():
    def __init__(self, save_dir: str, N_check_threads: int = 4):
        self.save_dir = save_dir
        self.url_list = []
        self.limit = 1000 # max 1000
        self.N_check_threads = N_check_threads
        # self.check_queue = queue.Queue()
        self.check_queue_list = []
        for i in range(N_check_threads): #
            self.check_queue_list.append(queue.Queue())
        self.save_queue = queue.Queue()
        # threading.Thread(target=self.check_worker,args=(self.check_queue,), daemon=True).start()
        threading.Thread(target=self.save_worker, args=(self.save_queue,), daemon=True).start()
        for i in range(N_check_threads):
            threading.Thread(target=self.check_worker, args=(self.check_queue_list[i],), daemon=True).start()
        # TODO: use N check_queue's for N-thread md5 checking; good for SSD
        # TODO: cleanup: remove check_queue
        
        return
    
    def check_worker(self, q:queue.Queue):
        while True:
            item = q.get() # (url, md5, save_filename)
            url, md5, save_filename = item
            
            with open(save_filename, 'rb') as file_to_check:
                data = file_to_check.read()
                md5_local_file = hashlib.md5(data).hexdigest()
    
            if md5_local_file == md5:
                logging.info('md5 equal: ' + save_filename)
                # END
            else:
                logging.warning('md5 not equal; re-downloading')
                self.save_queue.put(url, md5, save_filename)
            q.task_done()
        #return
    
    def save_worker(self, q:queue.Queue):
        while True:
            item = q.get() # (url, md5, save_filename)
            url, md5, save_filename = item
            
            self.try_save(url, md5, save_filename)
            q.task_done()
        #return
    
    @staticmethod
    def get_N_posts(url:str):
        success = False
        while not success:
            try:
                expanded_url_list = []
                headers = ''
                resp = requests.get(url, headers=headers)
                my_dict = xmltodict.parse(resp.content) # note: can fail here if server responds abnormally
                N_posts = int(my_dict['posts']['@count'])
                success = True
            except Exception as e:
                logging.error("ERROR : " + str(e))
                success = False
                time.sleep(1)
        return N_posts
    
    def add_url(self, url:str):
        self.url_list.append(url)
        return
    
    @staticmethod
    def save(url, save_filename):
        r = requests.get(url, timeout=15)
        if str(r.status_code) == str(200):
            with open(save_filename, 'wb') as f:  # xb: create file only; wb: (over)write
                f.write(r.content)
            
                # print('----------------')
                # print('saved to:', save_filename)
                # print('status code:', r.status_code)
                # print('content-type:', r.headers['content-type'])
                # print(r.status_code, r.headers['content-type'], 'saved to:', save_filename)
                logging.info(str(r.status_code) + " " + r.headers['content-type'] + ' saved to: ' + save_filename)
                # print(r.encoding)
        else:
            logging.info(str(r.status_code) + " " + r.headers['content-type'] + ' not saving')
        return
    
    @staticmethod
    def try_save(url, md5, save_filename):
        success = False
        while not success:
            try:
                Downloader.save(url, save_filename)
                
                # check local file md5 == md5 from query
                with open(save_filename, 'rb') as file_to_check:
                    data = file_to_check.read()
                    md5_local_file = hashlib.md5(data).hexdigest()

                if md5_local_file == md5:
                    success = True
                    # logging.info('md5 equal: ' + save_filename)
                else:
                    success = False
                    logging.warning('md5 not equal; re-downloading immediately')
                
                # success = True
            except Exception as e:
                # print("ERROR : " + str(e))
                logging.error("ERROR : " + str(e))
                success = False
                logging.warning('could not save; waiting to retry in 10s; {0} --> {1}'.format(url, save_filename))
                time.sleep(10)
        return
    
    def download(self, query_url:str):
        save_dir = self.save_dir
        
        # print('start downloading: ' + query_url)
        logging.info('start processing: ' + query_url)
        
        success = False
        while not success:
            try:
                
                headers = ''
                resp = requests.get(query_url, headers=headers) # don't need: wrap requests in "try until succeed"
                # print(resp.status_code)
                # print(resp.content)
                # print(resp)
        
                my_dict = xmltodict.parse(resp.content)
                # # pprint.pprint(my_dict, indent=2)
                # # file_url = my_dict['posts']['post']['@file_url']
                # # print(file_url)
                success = True
            except Exception as e:
                logging.error("ERROR : " + str(e))
                success = False
                time.sleep(1)

        # total number of posts for the query
        # count = my_dict['posts']['@count']
        # print('count:', count)
        # logging.info('count: ' + count)

        # if using limit=N, only N results will be returned:
        # https://yande.re/post.xml?tags=tony_taka&limit=2
        # to go to next page, do:
        # https://yande.re/post.xml?tags=tony_taka&limit=2&page=2

        download_list = []
        md5_list = []
        #N_posts = int(my_dict['posts']['@count'])  # len(my_dict['posts']['post'])  # number of posts returned
        N_posts = self.get_N_posts(query_url)
        # logging.info('count: ' + str(N_posts))
        if N_posts > 0:
            # print('total posts returned:', N_posts)
            logging.info('total posts returned: ' + str(N_posts))
            if N_posts > 1:
                for p in my_dict['posts']['post']:
                    download_list.append(p['@file_url'])
                    md5_list.append(p['@md5'])
                    # print(p['@file_url'])
            elif N_posts == 1:
                download_list.append(my_dict['posts']['post']['@file_url'])
                md5_list.append(my_dict['posts']['post']['@md5'])

        else:
            logging.info('count <= 0; could not find any posts')

        # print(download_list)

        logging.info('downloading...')
        for url, md5 in zip(download_list, md5_list):
    
            save_filename = url.split('/')[-1]
            save_filename = urllib.parse.unquote(save_filename)  # fix %20, and other %xx
            save_filename = save_filename.replace("?", "_") # replace invalid characters with _
            save_filename = save_filename.replace(":", "_")
            save_filename = save_filename.replace("*", "_")
            save_filename = save_filename.replace("\"", "_")
            save_filename = os.path.join(save_dir, save_filename)
            
            # give to worker threads
            if not os.path.exists(save_filename):
                self.save_queue.put((url, md5, save_filename))
            else: # exists
                # self.check_queue.put((url, md5, save_filename))
                k = random.randint(0,self.N_check_threads-1)
                self.check_queue_list[k].put((url, md5, save_filename)) # put onto a random queue
            
            # TODO: deprecate this if/else; use worker threads instead
            # if not os.path.exists(save_filename):  # if does not exist, save
            #     # TODO: BEGIN: replace with self.try_save()
            #     self.try_save(url, md5, save_filename)
            #     # r = requests.get(url)
            #     # if str(r.status_code) == str(200):
            #     #     with open(save_filename, 'xb') as f:  # create file only
            #     #         f.write(r.content)
            #     #
            #     #         # print('----------------')
            #     #         # print('saved to:', save_filename)
            #     #         # print('status code:', r.status_code)
            #     #         # print('content-type:', r.headers['content-type'])
            #     #         # print(r.status_code, r.headers['content-type'], 'saved to:', save_filename)
            #     #         logging.info(str(r.status_code ) + " " + r.headers['content-type'] + ' saved to: ' + save_filename)
            #     #         # print(r.encoding)
            #     # else:
            #     #     logging.info(str(r.status_code) + " " + r.headers['content-type'] + ' not saving')
            #     # END: replace with self.try_save()
            # else:  # exists
            #     # print('exists; checking md5')
            #     # check md5
            #     with open(save_filename, 'rb') as file_to_check:
            #         data = file_to_check.read()
            #         md5_local_file = hashlib.md5(data).hexdigest()
            #
            #     if md5_local_file == md5:
            #         # print('md5 equal:', save_filename)
            #         logging.info('md5 equal: ' + save_filename)
            #     else:
            #         logging.warning('md5 not equal; re-downloading')
            #         # TODO: BEGIN: replace with self.try_save()
            #         self.try_save(url, md5, save_filename)
            #         # r = requests.get(url)
            #         # if str(r.status_code) == str(200):
            #         #     with open(save_filename, 'wb') as f:  # overwrite file
            #         #         f.write(r.content)
            #         #
            #         #         # print('----------------')
            #         #         # print('saved to:', save_filename)
            #         #         # print('status code:', r.status_code)
            #         #         # print('content-type:', r.headers['content-type'])
            #         #         # print(r.status_code, r.headers['content-type'], 'saved to:', save_filename)
            #         #         logging.info(str(r.status_code) + " " + r.headers['content-type'] + ' saved to: ' + save_filename)
            #         # else:
            #         #     logging.info(str(r.status_code) + " " + r.headers['content-type'] + ' not saving')
            #         # END: replace with self.try_save()
        # print('finished downloading: ' + query_url)
        logging.info('finished processing: ' + query_url)
        return
    
    def download_list(self):
        for url in self.url_list: # for every url,
            
            # run query, get N_posts; expand url via page=
            # ex. limit=1000; N_count=1900; <url> --> <url>&page=1, <url>&page=2
            N_posts = self.get_N_posts(url)

            expanded_url_list = []
            page = 1
            while N_posts > 0:
                expanded_url_list.append(url + "&page={0}".format(page))
                N_posts -= self.limit
                page += 1
            
            logging.info(expanded_url_list)
            for url2 in expanded_url_list: # download each page
                self.download(url2)
        return
    
    def parse_text_file(self, file:str):
        '''parse .txt file and add to download list'''
        
        with open(file) as f:
            lines = [line.rstrip() for line in f]
        
        for L in lines:
            if L:
                if L[0] != "#":
                    url = "https://yande.re/post.xml?limit={0}&tags=".format(self.limit) + L
                    self.add_url(url=url)
                    # print('adding:', L)
                    logging.info('adding: ' + L)
        return


# url = "https://yande.re/post.xml?tags=tony_taka&limit=1000" # max limit is 1000
# url = "https://yande.re/post.xml?tags=kase_daiki&limit=1000&page=2"

# x = Downloader(save_dir=save_dir)
# x.add_url(url="https://yande.re/post.xml?limit=1000&tags=zukky")
# x.add_url(url="https://yande.re/post.xml?limit=1000&tags=")

# x.parse_text_file(file='C:/Users/Vincent Chyn/Desktop/DanbooruDownloader list.txt')
# x.download_list()

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('file', type=str)
    parser.add_argument('save_dir', type=str)
    args = parser.parse_args()
    
    # save_dir = os.path.join('D:', 'yande.re')
    save_dir = args.save_dir
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    
    x = Downloader(save_dir=save_dir)
    x.parse_text_file(file=args.file)
    x.download_list()
    
    # block until done
    for i in range(x.N_check_threads):
        x.check_queue_list[i].join()
    x.save_queue.join()
    print('-- done')

# TODO: query my favorites
# artists_list = []
# do query: url = "https://yande.re/post.xml?tags=order%3Avote+vote%3A3%3Aeccdbb"
# for each favorite post, if artists doesn't exist in artists_list, artists_list.append()
#
# for artist in artists_list:
#     download_list = []
#     set download dir
#     create artist folder if doesn't exist
#     do query: url = "https://yande.re/post.xml?tags=" + artist
#     for each post, add @file_url to download_list
#     try: downloading list and saving to disk; also log result to log.txt # ex. {saved to/couldn't save to}: <full file path>


# read list from list.txt
# then process list; including multi-page queries
#
# artist_list = []  # given
#
# for artist in artist_list:
#     # get count
#     url0 = "https://yande.re/post.xml?tags=%s" % artist
#     ...
#     count = ___
#
#     page = 1
#     cursor = count
#     while cursor > 0
#         url = "https://yande.re/post.xml?tags=kase_daiki&limit=1000&page=%.0f" & page
#
#         page += 1
#         cursor -= 1000

# TODO: use set() when ingesting .txt to prevent duplicates; also logging.warning('duplicate ignored from .txt: ___')